function calculate(){
  show(daysandyear(document.getElementById("data-days").value));
}

function daysandyear(days) {
  var ORIGINYEAR = 1980;
  var year = ORIGINYEAR;

  while (days > 365 + IsLeapYear(year)) {
      days -= 365 + IsLeapYear(year);
      year++;
    }
  return ({year: year, days: days});
}

function show(yearDay){
  document.getElementById("results-days").innerHTML = yearDay.days;
  document.getElementById("results-years").innerHTML = yearDay.year;
}

function IsLeapYear(year) {
  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}
